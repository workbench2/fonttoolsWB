"""
gui
===============================================================================

FontTools WorkBench GUI Application
"""

import sys

import fonttoolsWB as ftwb

app = ftwb.App(
    debug=0,
    iconName="FONTTOOLS",
)
del ftwb

def main():
    sys.exit(app.run())


if __name__ == "__main__":
    main()
