# fonttoolsWB

## FontTools Workbench
A simple application which provides a GUI for the famous 
[fontTools](https://pypi.org/project/fonttools/) package.
It is build with the [Workbench](https://pypi.org/project/wbBase/) RAD framework.

## Installation

```shell
pip install fonttoolsWB
```

If you intend to write your own Python scripts for the FontTools Workbench, 
you may want to install additional plugins to aid in development. 
This can be done as follows:
```shell
pip install fonttoolsWB[develop]
```

After installation you find an executable in the usual location:
- On Windows:
    
    `C:\Python310\Scripts\fonttoolswb.exe`

- On Mac:

    `Lib/Frameworks/Python.framework/Versions/3.10/bin/fonttoolswb`

The actual executable path may vary depending on your Python version 
and installation location.

Launch the executable to run the application.

## Documentation

For details read the [Documentation](https://workbench2.gitlab.io/fonttoolsWB/).

## Sample

Some sample scripts that you can run in the FontTools Workbench can be found 
in the [snippets](https://gitlab.com/workbench2/fonttoolsWB/-/snippets) section 
of the repository.
